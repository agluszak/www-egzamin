function generateHtml(json) {
    return `<tr>
            <td><a href="pokemon?id=${json.id}">${json.name}</a></td>
            <td>${json.height}</td>
            <td>${json.weight}</td>
            <td>${json.dangerous ? "tak" : "nie"}</td>
        </tr>`
}

function compareName(a, b) {
    return a.name<b.name?-1:(a.name>b.name?1:0);
}

function compareWeight(a, b) {
    return a.weight - b.weight;
}

function compareHeight(a, b) {
    return a.height - b.height;
}

let lastType = "";
let reversed = false;

function setHtml(json) {
    let html = json.map(pokemon => generateHtml(pokemon)).join("\n");
    let htmlElement = document.getElementById('table-header');
    htmlElement.insertAdjacentHTML('afterend', html);
}

function sortBy(type) {
    let json = JSON.parse(sessionStorage.getItem('list'));
    if (type === "name") {
        json.sort(compareName);
    } else if (type === "weight") {
        json.sort(compareWeight)
    } else if (type === "height") {
        json.sort(compareHeight)
    }
    if (lastType === type) {
        reversed = !reversed;
    } else {
        reversed = false;
    }
    lastType = type;
    if (reversed) {
        json.reverse();
    }
    setHtml(json);
}

fetch('http://localhost:4444/api/list')
    .then(response => response.json())
    .then(json => {
        sessionStorage.setItem('list', JSON.stringify(json));
        setHtml(json);
    });