function generateHtml(json) {
    return `<dl>
                <dt>Nazwa</dt>
                <dd>${json.name}</dd>
                <dt>Waga</dt>
                <dd>${json.weight}</dd>
                <dt>Wzrost</dt>
                <dd>${json.height}</dd>
                <dt>Typy</dt>
                <dd>${json.types.map(type => type.type_id).join(", ")}</dd>
            </dl>
            <b>${json.dangerous ? "Groźny" : "Niegroźny"}</b>`
}

function getQueryVariable(variable) {
    let query = window.location.search.substring(1);
    let vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split("=");
        if (pair[0] === variable) {
            return pair[1];
        }
    }
    return undefined;
}

let id = getQueryVariable("id");
let htmlElement = document.getElementsByTagName('main')[0];

if (id) {
    fetch('http://localhost:4444/api/get/' + id)
        .then(response => response.json())
        .then(json => {
            if (json) {
                return generateHtml(json)
            } else {
                throw "No id";
            }
        })
        .then(html => {
            htmlElement.insertAdjacentHTML('afterbegin', html);
        }).catch(e => htmlElement.insertAdjacentHTML('afterbegin', "<span>Wrong id</span>")
    );
} else {
    htmlElement.insertAdjacentHTML('afterbegin', "<span>Wrong id</span>");
}