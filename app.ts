import * as express from "express";
import * as cookieParser from "cookie-parser";
import * as logger from "morgan";
import * as path from "path";
import * as csurf from "csurf";
import {DAO} from "./DAO";

const app = express();

const port = 4444;

const csrfMiddleware = csurf({
    cookie: true
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(csrfMiddleware);
app.use(express.static(path.join(__dirname, 'public')));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

const dao = new DAO("baza.db");

interface Type {
    type_id: string;
    slot: number;
}

interface Pokemon {
    id: number;
    name: string;
    height: number;
    weight: number;
    types: Type[];
    dangerous: boolean;
}

interface Dangerous {
    id: number;
    pokemon_id: number;
    time: number;
}

app.get("/", (req, res) => {
    res.render("list");
});

app.get("/pokemon", (req, res) => {
   res.render("pokemon", {csrf: req.csrfToken(), id: req.query.id})
});

app.get("/api/list", async (req, res) => {
    let sql = `
    WITH dang as (
        SELECT COUNT(*) as count, pokemon_id FROM dangerous WHERE time > ? GROUP BY pokemon_id
    ) SELECT * FROM pokemon LEFT JOIN dang on dang.pokemon_id = pokemon.id
    `;
    let pokemons = await dao.all(sql, [Date.now() - 60*60*1000]) as any[];
    pokemons.map(x => {
        x.dangerous = x.count > 3;
        return x;
    });
    res.send(pokemons);
});

app.get("/api/get/:id", async (req, res) => {
    let id = req.params["id"];
    let pokemon = await dao.get(`SELECT * FROM pokemon where id = ?`, [id]) as Pokemon|undefined;
    if (pokemon) {
        pokemon.types = await dao.all(`SELECT * FROM pokemon_types where pokemon_id = ?`, [id]) as Type[];
        let dangerous = await dao.all(`SELECT * FROM dangerous where pokemon_id = ?`, [id]) as Dangerous[];
        dangerous.filter(value => value.time > Date.now() - 60*60*1000);
        pokemon.dangerous = dangerous.length >= 3;
    }
    res.send(pokemon);
});

app.post("/api/dangerous", async (req, res) => {
    let id = req.body._id;
    console.log("dangerous " + id);
    await dao.insert("INSERT INTO dangerous (pokemon_id, time) VALUES (?, ?)", [id, Date.now()]);
    res.status(204).send();
});

app.listen(port, () => {
    console.log('app listening on port: ' + port);
});


module.exports = app;
