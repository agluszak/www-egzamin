CREATE TABLE IF NOT EXISTS dangerous (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                pokemon_id INTEGER NOT NULL,
                time INTEGER NOT NULL
            );
