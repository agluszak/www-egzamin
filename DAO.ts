import {Database, verbose} from "sqlite3";

export class DAO {
    private db: Database;

    constructor(dbFilePath: string) {
        this.db = new (verbose().Database)(dbFilePath, (err) => {
            if (err) {
                console.log('Could not connect to database', err);
            } else {
                console.log('Connected to database');
            }
        })
    }

    run<Args>(sql: string, params: Args): Promise<void> {
        return new Promise((resolve, reject) => {
            this.db.run(sql, params, function (err) {
                if (err) {
                    console.log('Error running sql ' + sql);
                    console.log(err);
                    reject(err);
                } else {
                    resolve();
                }
            });
        })
    }

    insert<Args>(sql: string, params: Args): Promise<number> {
        return new Promise((resolve, reject) => {
            this.db.run(sql, params, function (err) {
                if (err) {
                    console.log('Error running sql ' + sql);
                    console.log(err);
                    reject(err);
                } else {
                    resolve(this.lastID);
                }
            });
        })
    }

    change<Args>(sql: string, params: Args): Promise<number> {
        return new Promise((resolve, reject) => {
            this.db.run(sql, params, function (err) {
                if (err) {
                    console.log('Error running sql ' + sql);
                    console.log(err);
                    reject(err);
                } else {
                    resolve(this.changes);
                }
            });
        })
    }

    get<Args, Result>(sql: string, params: Args): Promise<Result | undefined> {
        return new Promise((resolve, reject) => {
            this.db.get(sql, params, (err, result) => {
                if (err) {
                    console.log('Error running sql: ' + sql);
                    console.log(err);
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        })
    }

    all<Args, Result>(sql: string, params: Args): Promise<Result[]> {
        return new Promise((resolve, reject) => {
            this.db.all(sql, params, (err, rows) => {
                if (err) {
                    console.log('Error running sql: ' + sql);
                    console.log(err);
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    }

}
