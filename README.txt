Uruchamia się: "ts-node app.ts"

1.
Wykorzystałem szkielet aplikacji expressowej w typescripcie
Wykorzystałem klasę DAO ze swojego projektu zaliczeniowego (https://gitlab.com/agluszak/www-cosmic-shopkeeper-server/blob/master/src/utils/DAO.ts)
Dodałem dwie klasy css: only-wide i only-narrow, które wyświetlają swoją zawartość tylko jeśli jest odpowiednia szerokość
HTML przerobiłem na semantyczny (tagi header, footer, main, aside, nagłówki)
Serwer serwuje pliki w ejs

2.
Lista pokemonów jest pamiętana w sessionStorage, przy sortowaniu jest z niego wyciągana i odpowiednio sortowana.
Każda kolumna ma onclick listenera

3.
Serwer udostępnia restowe endpointy /api/list i /api/pokemon/:id
Klient fetchuje dane z endpointów i podmienia sobie html

4.
Dodałem nową tabelę (sql do jej stworzenia jest w pliku dangerous.sql) w której pamiętam zgłoszenia groźności postaci (id pokemona, czas zgłoszenia)
Jak wyciągam dane o pokemonie to sięgam do tej tabeli i sprawdzam czy w ciągu ostatniej godziny ilość zgłoszeń jest większa niż 3
Po wysłaniu formularza strona nie odświeża się